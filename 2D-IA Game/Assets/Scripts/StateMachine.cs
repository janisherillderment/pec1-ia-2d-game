﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{

    private State ActualState;
    [SerializeField]
    private NPC npc;
    [SerializeField]
    private Player player;


    void Start(){
        ActualState = new State();
       ActualState.InitiateState(npc, player);
    }

    public void SetState(string stateName){
        // if ( ActualState != null ){
        ActualState.SetBehaviour(stateName);
        ExecuteActualState();
        // }
        
    }
    // Retorno de estado
    public State GetState(){
        return ActualState;
    }

    public void ExecuteActualState(){
        ActualState.ExecuteBehaviour();
    }
    
}
