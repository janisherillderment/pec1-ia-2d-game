﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{
public NavMeshAgent agent;
[SerializeField]
private AudioClip sfx_door;

void Start(){
// Al comienzo de la escena se incializa la velocidad del jugador a 0 para indicar que el objeto está quieto.
// Además se obtiene la referencia al Animator que asigna animaciones según Idle/Run
    agent.speed = 0;
   var rotationVector = transform.rotation.eulerAngles;
   rotationVector.x = 0;
   rotationVector.y = 0;
   rotationVector.z = 0;
   transform.rotation = Quaternion.Euler(rotationVector);
}


 void Update()
 {
 Transform  parent;
 GameObject mainCamera;
 parent = transform.parent;

 mainCamera = GameObject.Find("Main Camera");
 var CameraZ = mainCamera.transform.eulerAngles.x;   
 transform.eulerAngles = new Vector3(-CameraZ, 0, 0);
 
 parent.eulerAngles = new Vector3(0, CameraZ, 0);
 Collider[] targetsInRadius = Physics.OverlapSphere(transform.position, 1, -1);

 GameObject door;  
 GameObject gem; 

void playSoundOpenDoor(){                   
     if(!GetComponent<AudioSource>().isPlaying){

         GetComponent<AudioSource>().PlayOneShot(sfx_door);     
      }
 }



 for ( int i = 0; i < targetsInRadius.Length; i++){
     switch(targetsInRadius[i].gameObject.name){
     case "SoulFragment A":
        door = GameObject.Find("DoorUp_NoGround A");
        door.active = false;
        gem = GameObject.Find("SoulFragment A");
        gem.active = false;
     break;
     case "SoulFragment B":
        door = GameObject.Find("Door_NoGround A");
        door.active = false;
        door = GameObject.Find("DoorUp_NoGround B");
        door.active = false;
        gem = GameObject.Find("SoulFragment B");
        gem.active = false;
     break;
     case "SoulFragment C":
        door = GameObject.Find("Door_NoGround B");
        door.active = false;
        gem = GameObject.Find("SoulFragment C");
        gem.active = false;
     break;
     case "SoulFragment D":
        gem = GameObject.Find("SoulFragment D");
        gem.active = false;
        door = GameObject.Find("Door_NoGround E");
        door.active = false;
     break;
     case "SoulFragment E":
        door = GameObject.Find("DoorUp_NoGround D");
        door.active = false;
        gem = GameObject.Find("SoulFragment E");
        gem.active = false;
     break;
     case "SoulFragment F":
        door = GameObject.Find("Door_NoGround C");
        door.active = false;
        gem = GameObject.Find("SoulFragment F");
        gem.active = false;
     break;
     case "SoulFragment G":
        door = GameObject.Find("DoorUp_NoGround C");
        door.active = false;
        gem = GameObject.Find("SoulFragment G");
        gem.active = false;
     break;
     case "Princess":
      SceneManager.LoadScene("MainMenu");
     break;
     default:

     break;
     }
     }
 if (!agent.pathPending)
 {
     if (agent.remainingDistance <= agent.stoppingDistance)
     {
         if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
         {
             agent.speed = 0f;
         }
     }
 }



 if (Input.GetMouseButtonDown(0)){
 

    agent.speed = 3.5f;
    RaycastHit hit;
    Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
    if (Physics.Raycast(camRay, out hit, 100))
    {   

        agent.destination = hit.point;
    }
 
 }

 }
 }
