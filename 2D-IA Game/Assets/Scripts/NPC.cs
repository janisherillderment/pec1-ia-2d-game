﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class NPC : MonoBehaviour
{
[SerializeField]
private StateMachine stateMachine;
public  NavMeshAgent agent;
private Vector3 lastTargetPos;
public float wanderRadius;
public float wanderTimer  = 10.0f;
public float viewRadius;
public float viewAngle;
public bool isDebuff;
private bool isReadyToMove;
private Vector3 NewCenter;
private float timeCounter;
private Transform target;
[SerializeField]
private AudioClip audioDetection;




 
 

    void Start () {

    

        agent = GetComponent<NavMeshAgent>();
        agent.speed = 2f;
        timeCounter = wanderTimer;
        stateMachine.SetState("Wander");

    }


    public Vector3 GetLastTargetPos(){
        return lastTargetPos;
    } 
  

    public void Debuff(){
        isDebuff = true;
    }

    public NavMeshAgent GetAgent(){
        return agent;
    }

    public void Cleanse(){
        isDebuff = false;
    }



    public void getNewDest(bool follow){
        if(!follow){
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            agent.SetDestination(newPos);
        }else{
            SetLastTargetPos(target.position);
            agent.ResetPath();
            agent.SetDestination(target.position);
        }
    }

   // Se recoge última posición reconocida del Jugador.
   public void SetLastTargetPos(Vector3 targetPos){
        lastTargetPos = targetPos;
    } 

    // Se indica nuevo objetivo cómo última posición reconocida del jugador.
     public void returnToLastTargetPos(){       
            agent.SetDestination(GetLastTargetPos());
    }

    public void getAway(){
        
        Vector3 newPos = Vector3.Scale(target.position, new Vector3(-1f,1f,-1f)) ;
        agent.SetDestination(newPos);
        Debug.DrawRay(transform.position, newPos, Color.black);
       

    }
    void Update () {
        
        calculateVision();
        wanderTimer -= Time.deltaTime;
 
        if (wanderTimer <= 0.0f)
        {   

            isReadyToMove = true;
            wanderTimer = timeCounter;
            
        }
        if (isReadyToMove){
        
        if (((agent.speed == 0f) && !isDebuff )) {
            isReadyToMove = false;
            agent.ResetPath();
            stateMachine.SetState("Wander");
            agent.speed = 2f;
            
            wanderTimer = timeCounter;
        }
     
        if (!agent.pathPending)
        {
            Debug.Log(this.name + "No path pending");
            if (agent.remainingDistance <= agent.stoppingDistance)
             {
              
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {          
                    Debug.Log("Speed 0");
                    agent.speed = 0f;

                }
        }
         }
         }

    }
    void OnDrawGizmosSelected(){
                    
            Gizmos.DrawWireSphere(transform.position, viewRadius);
            Vector3 vectorA =  transform.right;
            vectorA =  Quaternion.AngleAxis(viewAngle-90, Vector3.up) * vectorA;
            Vector3 vectorB =  transform.right;
            vectorB =  Quaternion.AngleAxis(-viewAngle-90, Vector3.up) * vectorB;
            Debug.DrawRay(transform.position, vectorA*10, Color.green);
            
            Debug.DrawRay(transform.position, vectorB*10, Color.green);
            vectorA =  Quaternion.AngleAxis(-45f, Vector3.up) * vectorA;
            Debug.DrawRay(transform.position, vectorA, Color.red);
            Gizmos.DrawWireSphere(NewCenter, 2);
    }

    public void SetTarget(Transform Target){
        target = Target;
    }
    public void calculateVision(){

        Collider[] targetsInRadius = Physics.OverlapSphere(transform.position, viewRadius, 1<<9);
        if( targetsInRadius.Length == 0  && stateMachine.GetState().GetBehaviour() == "Follow"){

            Debug.Log("Scan");
            stateMachine.SetState("Scan");
            
            
        }
        
        for ( int i = 0; i < targetsInRadius.Length; i++){
      
            
            Transform Target = targetsInRadius[i].transform;
            Vector3 direction = (Target.position - transform.position);
            SetTarget(Target);
            Vector3 vectorAxis =  transform.forward;
            // vectorAxis = Quaternion.AngleAxis(45, Vector3.right) * vectorAxis; 
            Debug.DrawRay(transform.position, vectorAxis*10, Color.black);
         
            
   
             if( Vector3.Angle(vectorAxis, direction) < viewAngle/2) {
                 
                
                float distancia  = Vector3.Distance(transform.position, Target.position);
                Physics.Raycast(transform.position, direction, distancia, 1<<8);

                if(!Physics.Raycast(transform.position, direction, distancia, 1<<8)){
                    
                    Debug.DrawRay(transform.position, direction, Color.black);
                 
                    agent.speed = 2f;
                    stateMachine.SetState("Follow");
                    if( distancia < 0.5){
                        SceneManager.LoadScene("MainMenu");
                    }


                }
            }
        }

    }
     public static Vector2 RandomPointOnUnitCircle(float radius)
 {
     float angle = Random.Range (0f, Mathf.PI * 2);
     float x = Mathf.Sin (angle) * radius;
     float y = Mathf.Cos (angle) * radius;

     return new Vector2 (x, y);

 }

public void setNewCenter(Vector3 v){
    NewCenter = v;
}
    public Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {

        
           
        RaycastHit hit; 
        // Se utiliza como nuevo destino del agente un punto aleatorio de una esfera con offset igual a la distancia establecida
        // en el atributo del NPC.
        // Cada vez que se calcule este nuevo destino se generá una traladación del vector "forward" del objeto del NPC aplicando
        // un ángulo aleatorio, por lo que la esfera que se genere variará de posición en cada momento.
        
        Vector3 NewOrigin = origin+ ( Quaternion.AngleAxis(Random.Range(10f, 350f),Vector3.up)*  Vector3.forward) *dist;  
        Vector3 direction = NewOrigin - origin;
        Debug.DrawRay(origin, direction , Color.green);
        Vector3 randPoint = NewOrigin + Random.insideUnitSphere * 2;
        setNewCenter(NewOrigin);
        direction = randPoint - origin;
        NavMeshHit navHit;
        Debug.DrawRay(origin, direction , Color.green);
        
        if( NavMesh.SamplePosition(randPoint, out navHit, dist+2, layermask)){
            
            return navHit.position;
        }else{
            return origin;
        }



        
    }
} 