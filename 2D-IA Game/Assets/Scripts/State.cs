﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    private NPC npc;
    private Player PlayerIn;
    private string behaviourTag;



    public void InitiateState(NPC Npc, Player player)
    {
        npc = Npc;
        PlayerIn = player;
        behaviourTag = "Wander";
    }

    // Indicar comportamiento del estado actual
    public void SetBehaviour(string tag){
        behaviourTag = tag;
    }
    public string GetBehaviour(){
        return behaviourTag;
    }


    public void ExecuteBehaviour(){
        Debug.Log("Cambio a estado: " + behaviourTag);
        switch(behaviourTag){

            
            case "Wander":
            npc.getNewDest(false);
            break;

            case "Follow":
            Debug.Log("Follow");
            npc.getNewDest(true);
            break;
 
            case "Away":
            npc.getAway();
            break;

            case "Scan":
            npc.returnToLastTargetPos();
            break;
            default: behaviourTag = "Wander";
            break;
        }

    }

}
